import gzip, os, re
#openPDBFile, checkResolution, pathFinder, getXYZ, getID, getAtomID, getResName
#toPdb, getCON, getProteinAndWater

def openPDBFile(protein_name, resLim = 2.0):
	pdb = []
	res = None
	flag = False
	name = pathFinder(protein_name)
	if os.path.exists(name):
		with gzip.open(name,'rt') as f:
			for line in f:
				if flag == False:
					res = checkResolution(line)
					flag = True
					if res > resLim:
						return None
				pdb.append(line)

		return pdb
	else:
		return None




def getProteinAndWaterTest(path):
	file = open(path)
	protein = []
	water = []
	for line in file:
		if len(line) < 78:
			continue
		if (line[17:20] == "HOH" and line[77] == "O"):
			water.append(getXYZ(line))
		if (line[16] != "A" and line[16] != " ") or (line[27] != "A" and line[27] != " "):
			continue
		if (line[17:20] != "HOH" and line[0:4] == "ATOM"):
			protein.append(line)
	file.close()
	return [protein, water]


def checkResolution(line):
	res = 0
	if ('RESOLUTION.' in line) and ('ANGSTROMS' in line):
		res = [float(s) for s in re.findall(r'-?\d+\.?\d*', line)][1]
	return res

def pathFinder(protein_name):
	path = '/home/artur/a/pdb/' + protein_name[1].lower() + protein_name[2].lower() + '/pdb' + protein_name.lower() + '.ent.gz'
    #path = '/home/artur/pdb/pdb/' + protein_name[1].lower() + protein_name[2].lower() + '/pdb' + protein_name.lower() + '.ent.gz'
	return path

def getXYZ(line):
	x = float(line[30:38])
	y = float(line[38:46])
	z = float(line[46:54])

	return [x, y, z]


def getID(line):
	i = int(line[6:11])
	return i

#Get atom's name
def getAtomID(line):
	i = line[13:16]
	return i

#Get amino acids name
def getResName(line):
	i = line[17:20]
	return i

def getProteinAndWater(pdb):
	protein = []
	water = []
	proteinCoords = []
	for line in pdb:
		if len(line) < 78:
			continue
		if (line[17:20] == "HOH" and line[77] == "O"):
			water.append(getXYZ(line))
		if (line[16] != "A" and line[16] != " ") or (line[27] != "A" and line[27] != " "):
			continue
		if (line[17:20] != "HOH" and line[0:4] == "ATOM"):
			protein.append(line)
	return protein, water

def getProteinAndWaterGRID(pdb):
	protein = []
	water = []
	proteinCoords = []
	for line in pdb:
		if len(line) < 78:
			continue
		if (line[17:20] == "HOH" and line[77] == "O"):
			water.append(line)
		if (line[16] != "A" and line[16] != " ") or (line[27] != "A" and line[27] != " "):
			continue
		if (line[17:20] != "HOH" and line[0:4] == "ATOM"):
			protein.append(line)
	return protein, water
	
def toPdb(xyz,B= 0, atom_id=None, res_id="  0", element= "U"):
    if (B>999):
        B = 999.0
    x = "{:8.3f}".format(xyz[0])
    y = "{:8.3f}".format(xyz[1])
    z = "{:8.3f}".format(xyz[2])
    B = "{:6.2f}".format(float(B))

    if res_id != "  0":
        if res_id>999:
            res_id-=int(res_id/1000)*1000
        res_id = str(res_id)
        res_id = " ".join([""]*(3-len(res_id)))+res_id
    line = "ATOM         "+element+"   "+element * 3+"    "+res_id+"     42.931 -14.533  18.887        0.00           "+element
    line_ = line[:30]+x+y+z+line[54:60]+B+line[66:]+"\n"
    return line_

def getCON(protein): #carbon oxygen nitro
	C = []
	O = []
	N = []

	for i in protein:
		if i[13] == 'O':
			O.append(getXYZ(i))
		if i[13] == 'C':
			C.append(getXYZ(i))
		if i[13] == 'N':
			N.append(getXYZ(i))
	return [C, O, N]
