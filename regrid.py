import os, sys, time, gzip, re, random
import numpy as np
import pandas as pd
from scipy.spatial import distance
import xgboost as xgb
from termcolor import colored
import pdbParser as prs
from sklearn.metrics import accuracy_score as acs
from sklearn.ensemble import RandomForestClassifier as rfc
from sklearn.svm import SVC as svc
import regrid_v2 as v2
import grid as g
import math
from sklearn.preprocessing import LabelEncoder
#v2.getplane(protein) = hba, hbd
#v2.hbs(hba, hbd, wat) = v_o, v_h


def printXY(x, y, protsUsed, testProts, max):
	global stri
	f = open('log.out', 'w')
	print x.shape
	print y.shape
	f.write(str(max) + '\n')
	f.write('x shape ' + str(x.shape[0]) + ' ' + str(x.shape[1]) + '\n')
	f.write('y shape ' + str(y.shape[0]) + '\n')
	f.write('prots used \n')
	f.write(stri + '\n')
	for i in protsUsed:
		f.write(i + ' ')
	f.write('\ntest prots \n')
	for i in testProts:
		f.write(i+ ' ')
	f.close()
	np.savetxt('testX.out', x)
	np.savetxt('testY.out', y)
#openPDBFile, checkResolution, pathFinder, getXYZ, getID, getAtomID, getResName
#toPdb, getCON, getProteinAndWater
def getDistanceBetween(chosenMolecule, molecules):
	return distance.cdist(chosenMolecule, molecules)
def getWatersArray(water):
	cd = getDistanceBetween(water, water)
	id_cd = np.where((cd < 4.0) & (cd > 0.0))
	cd_ = cd[id_cd]
	cd_.sort()
	return cd_
def getAtomsArrays(protein, water, lim = 8.0):
	[C, O, N] = prs.getCON(protein)
	cd_C = getDistanceBetween(water, C)
	cd_O = getDistanceBetween(water, O)
	cd_N = getDistanceBetween(water, N)
	id_cd_C = np.where(cd_C < lim)
	id_cd_O = np.where(cd_O < lim)
	id_cd_N = np.where(cd_N < lim)
	cd_C_ = np.array(cd_C[id_cd_C])
	cd_O_ = np.array(cd_O[id_cd_O])
	cd_N_ = np.array(cd_N[id_cd_N])
	cd_C_.sort()
	cd_O_.sort()
	cd_N_.sort()

	return cd_C_, cd_N_, cd_O_
def getAtomsForEachHOH(protein, waters, lim = 6.0):
	cdCON = []
	[C, O, N] = prs.getCON(protein)
	#print waters
	for water in waters:
		cdC = np.sort(getDistanceBetween([water], C)[0])
		cdO = np.sort(getDistanceBetween([water], O)[0])
		cdN = np.sort(getDistanceBetween([water], N)[0])
		cdCON.append([cdC[np.where(cdC < lim)], cdO[np.where(cdO < lim)], cdN[np.where(cdN < lim)]])

	return cdCON
def getAtomsForOnly(C, O, N, water, lim = 6.0):
	cdC = np.sort(getDistanceBetween([water], C)[0])
	cdO = np.sort(getDistanceBetween([water], O)[0])
	cdN = np.sort(getDistanceBetween([water], N)[0])
	return [cdC[np.where(cdC < lim)], cdO[np.where(cdO < lim)], cdN[np.where(cdN < lim)]]
def getFakeWater(water):
	cd = getDistanceBetween(water, water)
	fwater = []
	id_cd = np.where((cd < 4.0) & (cd > 0))

	idx = id_cd[0].tolist()
	jdx = id_cd[1].tolist()

	for i in range(len(idx)):
				if (jdx[i] > idx[i]):
					fwater.append(((np.array(water[idx[i]]) + np.array(water[jdx[i]]) )/ 2))
	return fwater
def filterProts(lim):
	file = open('bc-90.out', 'r')
	global forTest
	fprots = []

	count = 0
	for line in file:
		count += 1
		prots = line.split(" ")
		for i in prots:
			if len(i) > 6:
				prots.remove(i) #delete proteins like 3y3q_ai

		if lim >= len(prots):
			fprots.extend(prots)
		else:
			random = np.random.randint(len(prots), size= lim)
			for i in random:
				fprots.append(prots[i])

	return fprots
def filterWater(water, lim = 200):
	water_ = []
	if len(water) <= lim:
		water_ = water
	else:
		random = np.random.randint(len(water), size= lim)
		for i in random:
			water_.append(water[i])
	return water_
def prepareData(lim = 1, protsNum = 15, forTest = 5, nd = True, test = None):
	global protsUsed
	global testProts
	global testCON
	global testFWater
	if test != None:
		forTest = 0
	if nd == True:
		prots = filterProts(lim)
		random.shuffle(prots)
	else: 
		prots = protsUsed
	bondsP = []
	bondsN = []
	tbondsP = []
	tbondsN = []
	count = 0
	maxp = protsNum
	posData = []
	negData = []
	for i in prots:
		name = i[0:4]
		#print name
		if test != None:
			if name == test:
				continue
		pdb = prs.openPDBFile(name)
		if pdb == None:
			continue
		else:
			try:
				[protein, water] = prs.getProteinAndWater(pdb)
				water_ = water #filterWater(water)
				fwater = getFakeWater(water_)
				try:
					[v_oc, v_nh, v_oh] = v2.HBs(protein, water_)
					[v_oc1, v_nh1, v_oh1] = v2.HBs(protein, fwater)
				except KeyError:
					continue
				CON = getAtomsForEachHOH(protein, water_)
				CON1 = getAtomsForEachHOH(protein, fwater)				
				if (count <= maxp):
					for i in range(len(CON)):
						bondsP.append([v_oc[i], v_nh[i], v_oh[i]])
						posData.append(CON[i])						
					for j in range(len(CON1)):
						negData.append(CON1[j])
						bondsN.append([v_oc1[j], v_nh1[j], v_oh1[j]])
					protsUsed.append(name)
					
				elif (count > maxp):					
					testProts.append(name)
					for i in range(len(CON)):
						testCON.append(CON[i])
						tbondsP.append([v_oc[i], v_nh[i], v_oh[i]])
					for j in range(len(CON1)):
						testFWater.append(CON1[j])
						tbondsN.append([v_oc1[j], v_nh1[j], v_oh1[j]])
				
				count += 1
				if count == maxp + forTest:
					break
			except ValueError:
				continue
	#print posData, negData
	#print bondsP, bondsN, tbondsP, tbondsN
	print 'data prepared'
	return posData, negData, bondsP, bondsN, tbondsP, tbondsN
def balanceX(x):
	maxLen = max([len(i) for i in x])
	for i in x:
		if len(i) < maxLen:
			i = np.concatenate((np.zeros(maxLen - len(i)),i))
	print x.shape
	return x
	
def createXY(posData, negData, bondsP, bondsN, lim = 8, bondslim = 3):
	x = posData + negData
	b = bondsP + bondsN
	x_= []
	foo = []
	for i in range(len(x)): #in waters 
		foo = []
		for j in x[i]: # [C, O, N] to C + O + N
			if (len(j) >= lim):
				foo.extend(j[:lim])
			if (len(j) < lim):
				foo.extend([0]*(lim - len(j)))
				foo.extend(j)
		#poka po 1 dobavlyayu  b[i] = {Voc, Vnh, Von}   k = Vx
		for k in b[i]:
			foo.extend(k[0])
		x_.append(foo)
	y = np.zeros(len(x))
	for i in range(len(posData)):
		y[i] = 1
	return np.array(x_), y
	
	
def getXYZ(line):
	x = float(line[30:38])
	y = float(line[38:46])
	z = float(line[46:54])
	return [x, y, z]
	



def getMax(posData, negData, testCON, testFWater):
	x = posData + negData + testCON + testFWater
	maxlen = 0
	for i in x:
		maxx = len(max(i, key = len))
		if maxx > maxlen:
			maxlen = maxx
	return maxlen




#dodelat''''/////////////////////////////////
def createXYHist(posData, negData, bondsP, bondsN, bins = 10):
	x = posData + negData
	b = bondsP + bondsN
	x_ = []
	y = np.zeros(len(x))
	for i in range(len(posData)):
		y[i] = 1
	for i in range(len(x)):
		histC = np.histogram(x[i][0], bins= bins)
		histO = np.histogram(x[i][1], bins= bins)
		histN = np.histogram(x[i][2], bins= bins)
		foo = np.concatenate((histC[0], histO[0], histN[0]))
		for k in b[i]:
			foo.tolist().extend(k[0])
		x_.append(foo)
	return np.array(x_), y

def createX(CON, v_oc, v_nh, v_oh, lim = 8, bondslim = 3):
    bonds = [v_oc, v_nh, v_oh]

    data = CON
    foo = []
    for j in data: # [C, O, N] to C + O + N
        if (len(j) >= lim): 
            foo.extend(j[:lim])            
        if (len(j) < lim):
           

            foo.extend([0]*(lim - len(j)))
            foo.extend(j)
           
    #poka po 1 dobavlyayu  b[i] = {Voc, Vnh, Von}   k = Vx
    #posmotri kak viglyadit vihod c HBs s odnoy HOH
 
    for k in bonds:       
        foo.extend(k[0][0])

                
    return foo
def format(xyz, line):
    x = "{:8.3f}".format(xyz[0])
    y = "{:8.3f}".format(xyz[1])
    z = "{:8.3f}".format(xyz[2])
    line_ = line[:30]
    line_ += x
    line_ += y
    line_ += z+line[54:]+"\n"
    return line_

def shiftAtoms1(atoms, shift):
    xyz_ = []
    for line in atoms:
        line_ = []
        xyz = getXYZ(line) + shift        
        line_ = format(xyz,  line)    	
        #line_ = line[:30]+x+y+z+line[54:60]+b+line[66:]+"\n"
        xyz_.append(line_)
    return xyz_	    

def test():
    test = '1FJS'
    pdb = prs.openPDBFile(test)
    [pr, w] = prs.getProteinAndWaterGRID(pdb) #!xyz w
    [C, O, N] = prs.getCON(pr)
    [xyz, grid, shift] = g.getEmptyGrid(protein = pr ) 
    pr_ = shiftAtoms1(pr, shift)

    mwater = [1, 3, 5]
    CON = getAtomsForOnly(C, O, N, mwater)
    
    [v_oc, v_nh, v_oh] = v2.HBs(pr_, [mwater])
    xi = createX(CON, v_oc, v_nh, v_oh)
   
    """[C, O, N] = prs.getCON(pr)
    [xyz, grid, shift] = g.getEmptyGrid(protein = pr ) #shifted xyz coordinates
    w_ = g.shiftAtoms(w, shift)
    pr_ = shiftAtoms1(pr, shift)
    for i in range(len(grid)):
        for j in range(len(grid[i])):
            for k in range(len(grid[i][j])):
                mwater = [i, j, k]
                CON = getAtomsForOnly(C, O, N, mwater)
                [v_oc, v_nh, v_oh] = v2.HBs(pr_, mwater)
                xi = createX(CON, v_oc, v_nh, v_oh)
                
#               yp = model0.predict(xt0)"""

    
def getgridid(xyz, step = 1):
    return tuple((xyz / step).astype(int))

def train(posData, negData, bondsP, bondsN, tbondsP, tbondsN):
    global testProts
    global testCON #test posData
    global testFWater # test negData
    global protsUsed
  
    
    

#for water in w_:
#		[v_oc, v_nh, v_oh] = v2.HBs(pr_, water)
#		CON = getAtomsForOnly(pr_, water)
#		xi = createX(CON, v_oc, v_nh, v_oh)

    [x0, y0] = createXY(posData, negData, bondsP, bondsN)
    print x0.shape
    [xt0, yt0] = createXY(testCON, testFWater, tbondsP, tbondsN )
	#[x1, y1] = createXYHist(posData, negData, bondsP, bondsN)
	#[xt1, yt1] = createXYHist(testCON, testFWater, tbondsP, tbondsN)
	#printXY(x0, y0, protsUsed, testProts, maxlen)
	#xgboost
    model0 = xgb.XGBClassifier()
    model0.fit(x0, y0)
    model0.save_model('def.model')
    print xt0.shape    
    yp0 = model0.predict(xt0)
    predictions0 = [round(value) for value in yp0]
    print predictions0
	# evaluate predictions
    accuracy0 = acs(yt0, predictions0)
    
    f = open('log', 'w')
    test = '1FO9'
    pdb = prs.openPDBFile(test)
    [pr, w] = prs.getProteinAndWaterGRID(pdb) #!xyz w
    [C, O, N] = prs.getCON(pr)
    [xyz, grid, shift] = g.getEmptyGrid(protein = pr ) #shifted xyz coordinates
    w_ = g.shiftAtoms(w, shift)
    pr_ = shiftAtoms1(pr, shift)
    for i in range(len(grid)):
        for j in range(len(grid[i])):
            for k in range(len(grid[i][j])):
                mwater = [i, j, k]
                CON = getAtomsForOnly(C, O, N, mwater)
                [v_oc, v_nh, v_oh] = v2.HBs(pr_, mwater)
                xi = createX(CON, v_oc, v_nh, v_oh)
                y = model0.predict(xi)
                print i, j, k
                print colored(y, 'red')
                f.write(str(i) + ' ' + str(j) + ' ' + str(k) + ' ')  
    
    
    f.close()
    """
	#xgboost hist
	model1 = xgb.XGBClassifier()
	model1.fit(x1, y1)

	yp1 = model1.predict(xt1)
	predictions1 = [round(value) for value in yp1]
	# evaluate predictions
	accuracy1 = acs(yt1, predictions1)
	print colored('accuracy of prediction xgb hist ' + str(accuracy1), 'red')

	#svc
	model2 = svc(gamma= 'auto')
	model2.fit(x0, y0)
	yp2 = model2.predict(xt0)
	predictions2 = [round(value) for value in yp2]
	accuracy2 = acs(yt0, predictions2)
	print colored('accuracy of pred svm svc ' + str(accuracy2), 'red')

	#svc with hist
	model3 = svc(gamma= 'auto')
	model3.fit(x1, y1)
	yp3 = model3.predict(xt1)
	predictions3 = [round(value) for value in yp3]
	accuracy3 = acs(yt1, predictions3)
	print colored('accuracy of pred svm svc with hist ' + str(accuracy3), 'red')

	#random forest
	model4 = rfc()
	model4.fit(x0, y0)
	yp4 = model4.predict(xt0)
	predictions4 = [round(value) for value in yp4]
	accuracy4 = acs(yt0, predictions4)
	print colored('accuracy of pred rfc ' + str(accuracy4), 'red')
	#random forest hist
	model5 = rfc()
	model5.fit(x1, y1)
	yp5 = model5.predict(xt1)
	predictions5 = [round(value) for value in yp5]
	accuracy5 = acs(yt1, predictions5)
	print colored('accuracy of pred rfc with hist ' + str(accuracy5), 'red')
    """
	#print 'train complete'


def loadmodel():
    clf = xgb.XGBClassifier()
    model = xgb.Booster()

    
    model.load_model("def.model")
    clf._Booster = model
    clf._le = LabelEncoder().fit([0, 1])
    f = open('log', 'w')
    test = '1FO9'
    pdb = prs.openPDBFile(test)
    [pr, w] = prs.getProteinAndWaterGRID(pdb) #!xyz w
    [C, O, N] = prs.getCON(pr)
    [xyz, grid, shift] = g.getEmptyGrid(protein = pr ) #shifted xyz coordinates
    w_ = g.shiftAtoms(w, shift)
    pr_ = shiftAtoms1(pr, shift)
    for i in range(len(grid)):
        for j in range(len(grid[i])):
            for k in range(len(grid[i][j])):
                mwater = [i, j, k]
                CON = getAtomsForOnly(C, O, N, mwater)
                [v_oc, v_nh, v_oh] = v2.HBs(pr_, mwater)
                xi = createX(CON, v_oc, v_nh, v_oh)
                y = clf.predict(xi)
#print i, j, k
#               print colored(y, 'red')
                f.write(str(i) + ' ' + str(j) + ' ' + str(k) + ' ' + str(y[0]) + '\n')  
                
    
    f.close()

stri = ''

protsUsed = []
testProts = []
testCON = []
testFWater = []
#[posData, negData, bondsP, bon] = prepareData()
#print colored(len(protsUsed), 'red')
#[posData, negData, bondsP, bondsN, tbondsP, tbondsN] = prepareData()
#train(posData, negData, bondsP, bondsN, tbondsP, tbondsN)

#[posData, negData, bondsP, bondsN, tbondsP, tbondsN] = prepareData(test = '1FO9')

#train(posData, negData, bondsP, bondsN, tbondsP, tbondsN )
loadmodel()
