import regrid as v1
import regrid_v2 as v2
import pdbParser as prs
import xgboost as xgb
from sklearn.metrics import accuracy_score as acs


def waterNum(water, count):
	temp = False
	if len(water) == count:
		temp = True
	return temp
	





def prepareData(wnum, prots, lim = 1, forTest = 5):
	testCON = []
	testFWater = []
	bondsP = []
	bondsN = []
	tbondsP = []
	tbondsN = []
	count = 0
	maxp = 150
	posData = []
	negData = []
	
	for i in prots:
		name = i[0:4]
		pdb = prs.openPDBFile(name)
		if pdb == None:
			continue
		try:
			[protein, water] = prs.getProteinAndWater(pdb)
		except ValueError:
			continue
		if waterNum(water, wnum) == False:
			continue
		else:
			try:
				
				water_ = water
				fwater = v1.getFakeWater(water_)
				try:
					[v_oc, v_nh, v_oh] = v2.HBs(protein, water_)
					[v_oc1, v_nh1, v_oh1] = v2.HBs(protein, fwater)
				except KeyError:
					continue
				CON = v1.getAtomsForEachHOH(protein, water_)
				CON1 = v1.getAtomsForEachHOH(protein, fwater)
								
				if (count <= maxp):
					for i in range(len(CON)):
						bondsP.append([v_oc[i], v_nh[i], v_oh[i]])
						posData.append(CON[i])
						
					for j in range(len(CON1)):
						negData.append(CON1[j])
						bondsN.append([v_oc1[j], v_nh1[j], v_oh1[j]])
				elif (count > maxp):
	
					for i in range(len(CON)):
						testCON.append(CON[i])
						tbondsP.append([v_oc[i], v_nh[i], v_oh[i]])
					for j in range(len(CON1)):
						testFWater.append(CON1[j])
						tbondsN.append([v_oc1[j], v_nh1[j], v_oh1[j]])
				
				count += 1
				if count == maxp + forTest:
					break
			except ValueError:
				continue


	return posData, negData, bondsP, bondsN, tbondsP, tbondsN, testCON, testFWater
	
	
	
	
def train(posData, negData, bondsP, bondsN, tbondsP, tbondsN, testCON, testFWater, i):
	waternum = i
	[x0, y0] = v1.createXY(posData, negData, bondsP, bondsN)
	[xt0, yt0] = v1.createXY(testCON, testFWater, tbondsP, tbondsN )
	model0 = xgb.XGBClassifier()
	model0.fit(x0, y0)
	try:
		yp0 = model0.predict(xt0)
	
		predictions0 = [round(value) for value in yp0]
		# evaluate predictions
		accuracy0 = acs(yt0, predictions0)
	except ValueError:
		print xt0
		print xt0.shape	
	return accuracy0, waternum, y0.shape[0]

prots = v1.filterProts(1)
acc = None
w = None
n = None
f = open('log', 'w')

for i in range(20, 150):
	[posData, negData, bondsP, bondsN, tbondsP, tbondsN, testCON, testFWater] = prepareData(i, prots)
	[acc, w, n] = train(posData, negData, bondsP, bondsN, tbondsP, tbondsN, testCON, testFWater, i)
	print acc, w, n
	f.write(str(acc) + ' ' + str(w) + ' ' + str(n) + '\n')
f.close()
