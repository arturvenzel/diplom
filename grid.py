import os 
import sys 
import numpy as np
from scipy.spatial import distance
import itertools
import random


def saveGrid(grid, path= 't.pdb'):
	f = open(path, 'w')
	pdb = ""
	for idx, value in np.ndenumerate(grid):
		if value == -1:
			pdb += toPdb(idx, B= 1)
		if value == 1:
			pdb += toPdb(idx, B= 2)
		if value == 2:
			pdb += toPdb(idx, B= 3)
		if value == -2:
			pdb += toPdb(idx)
	f.write(pdb)
	f.close()


def toPdb(xyz,B= 0, atom_id=None, res_id="  0", element= "O"):
    if (B>999):
        B = 999.0
    x = "{:8.3f}".format(xyz[0])
    y = "{:8.3f}".format(xyz[1])
    z = "{:8.3f}".format(xyz[2])
    B = "{:6.2f}".format(float(B))
    if res_id != "  0":
        if res_id>999:
            res_id-=int(res_id/1000)*1000
        res_id = str(res_id)
        res_id = " ".join([""]*(3-len(res_id)))+res_id	    	
    line = "HETATM       "+element+"   "+"HOH"+"    "+res_id+"     42.931 -14.533  18.887        0.00           "+element
    line_ = line[:30]+x+y+z+line[54:60]+B+line[66:]+"\n"
    return line_
    
    
def getXYZ(line):
	x = float(line[30:38])
	y = float(line[38:46])
	z = float(line[46:54])
	return [x, y, z]

def getID(line):
	i = int(line[6:11])
	return i
	

	
def getDistanceBetween(chosenMolecule, molecules):
	return distance.cdist(chosenMolecule, molecules)

def getEmptyGrid( step = 1 , protein = None):
	xyz = []
	for i in protein:
		xyz.append(getXYZ(i))
	xyz = np.array(xyz)
	shift = np.absolute(np.amin(xyz, axis = 0))
	xyz += shift
	maxxyz = np.amax(xyz, axis = 0)
	numOfDotsX = int(maxxyz[0] / step + 1)
	numOfDotsY = int(maxxyz[1] / step + 1)
	numOfDotsZ = int(maxxyz[2] / step + 1)
	grid = np.zeros((numOfDotsX, numOfDotsY, numOfDotsZ))
	return [xyz, grid, shift]

def getIDOnGrid(xyz, step):
	return tuple((xyz / step).astype(int))

def shiftAtoms(atoms, shift):
	xyz = []
	for i in atoms:
		xyz.append(getXYZ(i) + shift)
	return xyz
	
def fillGrid(xyz, emptyGrid, protein, shift, atomID= None, step= 1):
	grid = emptyGrid
	if atomID == None:
		for i in xyz:
			idx = getIDOnGrid(i, step)
			grid[idx] = -2
	else:
		xyzAtom = getXYZ(protein[atomID]) 
		xyzAtom = np.array(xyzAtom + shift)
		dist = np.array(getDistanceBetween([xyzAtom], xyz)[0])
		id_dist = np.array(np.where((dist < 3) & (dist > 0)))[0]	

		idx = getIDOnGrid(xyzAtom, step)
		grid[idx] = -1
		for i in id_dist:		
			idt = getIDOnGrid(xyz[i], step)
			grid[idt] = 1
	
		id_dist = np.array(np.where((dist < 4) & (dist > 3)))[0]
		for i in id_dist:
			idt = getIDOnGrid(xyz[i], step)
			grid[idt] = 2
	return grid				



