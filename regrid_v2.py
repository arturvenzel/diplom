import pdbParser as prs
import xgboost as xgb
from scipy.spatial import distance
from operator import itemgetter
import numpy as np
from itertools import ifilter
from termcolor import colored
def getxyz(line):
    x = float(line[30:38])
    y = float(line[38:46])
    z = float(line[46:54])
    return np.array([x,y,z])
def getDist(water, mols):
	return distance.cdist(water, mols)

def getProt(path, singleChain = False):
    f = open(path)
    prot = []
    wat  = []
    if singleChain is True:
        cid = None
    for line in f:
        if len(line)<78:
            continue
        if singleChain is True:
            if cid is None:
                cid = line[21]
            if line[21]!=cid:
                continue
        if line[17:20]=="HOH" and line[77]=="O":
            wat.append(line)
        if line[16]!="A" and line[16]!=" ":
            continue
        if line[27]!="A" and line[27]!=" ":
            continue
        if line[17:20]!="HOH" and line[0:4]=="ATOM":
            prot.append(line)
    p = np.array([getxyz(p_) for p_ in prot])
    f.close()
    return [prot,p,wat]

def getcon(protein): #carbon oxygen nitro
	C = []
	O = []
	N = []
	for i in protein:
		if i[13] == 'O':
			O.append(i)
		if i[13] == 'C':
			C.append(i)
		if i[13] == 'N':
			N.append(i)
	return [C, O, N]


def getres(line):
	i = int(line[22:26])
	return i

def getName(line):
	i = line[13:16]
	return i
def getResName(line):
	i = line[17:20]
	return i
	
def getatomres(protein, resid):
	atoms = {}
	for line in protein:
		if getres(line) == resid:
			atoms[getName(line)] = line
	
	return atoms
def getnorm(plane):
	[p0, p1, p2] = plane
	[x0, y0, z0] = p0
	[x1, y1, z1] = p1
	[x2, y2, z2] = p2
	ux, uy, uz = u = [x1-x0, y1-y0, z1-z0]
	vx, vy, vz = v = [x2-x0, y2-y0, z2-z0]
	u_cross_v = [uy*vz-uz*vy, uz*vx-ux*vz, ux*vy-uy*vx]
	normal = np.array(u_cross_v) / np.linalg.norm(u_cross_v)
	return normal
	
def getdata(protein):
	hbs = []
	hba = []
	res = []
	#[c,d o, n] = getcon(protein)
	octype = {}
	octype = {"OE1": "CD ", "OE2": "CD " , "O  ": "C  ", "OD1" : "CG ", "OD2" : "CG "}
	other = []
	read = []
	
	for i in protein:
		if (getName(i) == "N  "):
			res = getres(i)
			atoms = getatomres(protein, res)
			plane = []
			a = getxyz(i) #N
			plane.append(a)
			b = getxyz(atoms["CA "])
			plane.append(b)
			c = getxyz(atoms["C  "])
			plane.append(c)
			
			hba.append([a, b, c, getnorm(plane)])
			
			
		#SER OG - CB - CA   a = OG  b = v(OG:CB) c = norm(CA OG CB)
		 	
		if (getName(i) == "OG "):
			res = getres(i)
			if getResName(i) == "SER":
				atoms = getatomres(protein, res)
				a = getxyz(i)
				b = getxyz(atoms["OG "]) - getxyz(atoms["CB "])
				c = getnorm([a, getxyz(atoms["CB "]), getxyz(atoms["CA "])])
				other.append([a, b, c, getResName(i)])		
			
			
		#TYR  HH - OH - CZ - CE1
		if (getName(i) == "OH "):
			res = getres(i)
			if getResName(i) == "TYR":
				atoms = getatomres(protein, res)
				a = getxyz(i)
				b = getxyz(atoms["OH "]) - getxyz(atoms["CZ "])
				c = getnorm([a, getxyz(atoms["CZ "]), getxyz(atoms["CE1"])])
				other.append([a, b, c, getResName(i)])
				
		#THR OG1 - HG1 - CB - CA
		if (getName(i) == "OG1"):
			res = getres(i)
			if getResName(i) == "THR":
				atoms = getatomres(protein, res)
				a = getxyz(i)
				b = getxyz(atoms["OG1"]) - getxyz(atoms["CB "])
				c = getnorm([a, getxyz(atoms["CB "]), getxyz(atoms["CA "])])
				other.append([a, b, c, getResName(i)])
		
		if (getName(i)) == "ND1":
			res = getres(i)
			if getResName(i) == "HIS":
				atoms = getatomres(protein, res)
				plane = []
				a = getxyz(i) #ND1
				plane.append(a)
				b = getxyz(atoms["CG "])
				plane.append(b)
				c = getxyz(atoms["CE1"])
				plane.append(c)			
				hba.append([a, b, c, getnorm(plane)])
				
		if (getName(i)) == "NE2":
			res = getres(i)
			if getResName(i) == "HIS":
				atoms = getatomres(protein, res)
				plane = []
				a = getxyz(i) #ND1
				plane.append(a)
				b = getxyz(atoms["CD2"])
				plane.append(b)
				c = getxyz(atoms["CE1"])
				plane.append(c)			
				hba.append([a, b, c, getnorm(plane)])
				
		if (getName(i)) == "NE1":

			res = getres(i)
			if getResName(i) == "TRP":
				atoms = getatomres(protein, res)
				plane = []
				a = getxyz(i) #NE1
				plane.append(a)
				b = getxyz(atoms["CD1"])
				plane.append(b)
				c = getxyz(atoms["CE2"])
				plane.append(c)
			
				hba.append([a, b, c, getnorm(plane)])
			
		if (getName(i) in octype):
			res = getres(i)
			atoms = getatomres(protein, res)
			plane = []		
			if getName(i) == "OE1":
				a = getxyz(atoms["OE1"])
				if ("OE2" in atoms):
					b = getxyz(atoms["OE2"])
				else:
					b = getxyz(atoms["NE2"])
				c = getxyz(atoms["CD "])
				plane.append(a)
				plane.append(b)
				plane.append(c)
				hbs.append([c, a, getnorm(plane), a - c])		
			if getName(i) == "OE2":
				a = getxyz(atoms["OE1"])
				b = getxyz(atoms["OE2"])
				c = getxyz(atoms["CD "])
				plane.append(a)
				plane.append(b)
				plane.append(c)
				hbs.append([c, a, getnorm(plane), b - c])	
			if getName(i) == "OD1":
				a = getxyz(atoms["OD1"])
				if ("OD2" in atoms):
					b = getxyz(atoms["OD2"])
				else:
					b = getxyz(atoms["ND2"])
				c = getxyz(atoms["CG "])
				plane.append(a)
				plane.append(b)
				plane.append(c)
				hbs.append([c, a, getnorm(plane), a - c])	
			if getName(i) == "OD2":
				a = getxyz(atoms["OD1"])
				b = getxyz(atoms["OD2"])
				c = getxyz(atoms["CG "])
				plane.append(a)
				plane.append(b)
				plane.append(c)
				hbs.append([c, a, getnorm(plane), b - c])	
			if getName(i) == "O  ":
				a = getxyz(atoms["O  "])
				b = getxyz(atoms["CA "])
				c = getxyz(atoms["C  "])
				plane.append(a)
				plane.append(b)
				plane.append(c)
				hbs.append([c, a, getnorm(plane), a - c])					
	return hbs, hba, other
	
#a = OG  b = v(OG:CB) c = norm(CA OG CB)	
#hbs = {hb[xyz_c, xyz_o, norm, o=c])}	
#hba = {ha[xyz_n, xyz_ca, xyz_c, norm])}
def HBs(prot,  wat):
	[hbs_, hba_, others] = getdata(prot)
	hbs = []
	hba = []
	ohs = []
	for w in wat:
		co_ = []
		nh_ = []
		oh_ = []
		for hb in hbs_:
			#wx = getxyz(w)
			wx = w
			wo = wx - hb[1]
			d = np.linalg.norm(wo) 
			if d > 4.0:
				continue			
			cosa = np.dot(wo, hb[3]) / (np.linalg.norm(wo) * (np.linalg.norm(hb[3]))) 
			cosb = np.dot(wo, hb[2]) / (np.linalg.norm(wo) * (np.linalg.norm(hb[2])))
			co_.append([np.linalg.norm(wo), cosa, cosb])		
		hbs.append(co_)
		#cos a = hx * norm = (nx - nh) * norm / |a| 
		
		#hba = {ha[xyz_n, xyz_ca, xyz_c, norm])}
		for ha in hba_:
			#wx = getxyz(w)
			wx = w
			wn = wx - ha[0]
			d = np.linalg.norm(wn)
			if d > 4.0:
				continue
			nh = 1.5 * ((ha[0] - ha[1]) + (ha[0] - ha[2])) / np.linalg.norm((ha[0] - ha[1]) + (ha[0] - ha[2])) + ha[0] 
			cosa = np.dot(ha[3], (wn - nh)) / (np.linalg.norm(ha[3]) * (np.linalg.norm(wn - nh))) 
			cosb = np.dot(nh, wx) / (np.linalg.norm(nh) * np.linalg.norm(wx))
			nh_.append([d, cosa, cosb]) 
		hba.append(nh_)
		
		
		#o - h
		for other in others:
			#wx = getxyz(w)
			wx = w
			wo = wx - other[0]
			d = np.linalg.norm(wo)
			if d > 4.0:
				continue
			cosa = np.dot(other[1], wo) / (np.linalg.norm(wo) * np.linalg.norm(other[1]))
			cosb = np.dot(other[2], wo) / (np.linalg.norm(wo) * np.linalg.norm(other[2]))
			oh_.append([d, cosa, cosb])
		ohs.append(oh_)
	for i in ohs:
		if not i:
			for j in range(3):
				i.append([0, 0, 0])
		if len(i) < 3:
			for j in range(3 - len(i)):
				i.append([0, 0, 0])	
						 
	for i in hba:
		if not i:
			for j in range(3):
				i.append([0, 0, 0])
		if len(i) < 3:
			for j in range(3 - len(i)):
				i.append([0, 0, 0])		
					
	for i in hbs:
		if not i:
			for j in range(3):
				i.append([0, 0, 0])
		if len(i) < 3:
			for j in range(3 - len(i)):
				i.append([0, 0, 0])		
				
				
	
	return hbs, hba, ohs



"""[prot, p, wat] = getProt('1fjs.pdb')

[hbs_, hba_, ohs_] = HBs(prot, wat)
for i in range(len(hbs_)):
	print colored(str(i) + '++++++++++++++++++++++++++++++++++++++++++++++++++' ,'cyan')
	
	print colored('C=0 ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~', 'red')
	print hbs_[i]
	print colored('N-H ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~', 'blue')
	print hba_[i]
	print colored('O-H ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~', 'yellow')
	print ohs_[i]
"""
	



